import Foundation
import UIKit

class ColorManager {
    
    static let shared = ColorManager()
    
    private init(){}
    
    let darkBlue = UIColor(red: 0.03, green: 0.31, blue: 0.47, alpha: 1)
    let lightBlue = UIColor(red: 0.52, green: 0.85, blue: 0.81, alpha: 1)
    let whiteColorForElements = UIColor(red: 1, green: 1, blue: 1, alpha: 0.6)
    let pinkWarning = UIColor(r: 208, g: 66, b: 157)
    let greenAccepting = UIColor(r: 113, g: 192, b: 94)
    let logoGray = UIColor(r: 58, g: 58, b: 58)
}
