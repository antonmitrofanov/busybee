import Foundation
import UIKit
import CoreLocation

extension UIView {
    
    func setGradientBackground(_ colorOne: UIColor, _ colorTwo: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [colorOne.cgColor, colorTwo.cgColor]
        gradientLayer.locations = [0.0, 0.75, 1]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
}

extension UIViewController {
    
    func setupController(viewsForGradient: [UIView]?, buttonsCollection: [UIButton]?, viewsCollection: [UIView]?, textFieldsCollection: [UITextField]?, color: UIColor?) {
        if let viewsForGradient = viewsForGradient {
            for view in viewsForGradient {
                view.setGradientBackground(ColorManager.shared.darkBlue, ColorManager.shared.lightBlue)
            }
        }
        if let buttonsCollection = buttonsCollection {
            for button in buttonsCollection {
                button.layer.cornerRadius = 20
                button.backgroundColor = .clear
                button.setTitleColor(.white, for: .normal)
                button.layer.borderWidth = 2
                button.layer.borderColor = UIColor.white.cgColor
            }
        }
        if let viewsCollection = viewsCollection {
            for view in viewsCollection {
                view.layer.cornerRadius = 20
                view.backgroundColor = ColorManager.shared.whiteColorForElements
            }
        }
        if let textFieldsCollection = textFieldsCollection {
            for textField in textFieldsCollection {
                let bottomLine = CALayer()
                bottomLine.frame = CGRect(x: 0.0, y: textField.frame.height, width: textField.frame.width, height: 2)
                if let color = color {
                    bottomLine.backgroundColor = color.cgColor
                    textField.textColor = color
                } else {
                    bottomLine.backgroundColor = UIColor.white.cgColor
                    textField.textColor = UIColor.white
                }
                textField.borderStyle = UITextField.BorderStyle.none
                textField.layer.addSublayer(bottomLine)
            }
        }
    }
    
    func makeButtonsRound(buttons: [UIButton]) {
        for button in buttons {
            button.layer.cornerRadius = button.frame.width/2
            button.backgroundColor = ColorManager.shared.whiteColorForElements
            let inset = button.frame.width/4
            button.imageEdgeInsets = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
            button.layer.borderWidth = 1
            button.layer.borderColor = ColorManager.shared.darkBlue.cgColor
        }
    }
        
}

extension UIColor {
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}

extension CLLocationCoordinate2D: Codable {
    public enum CodingKeys: String, CodingKey {
        case latitude
        case longitude
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.latitude, forKey: .latitude)
        try container.encode(self.longitude, forKey: .longitude)
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let latitude = try container.decodeIfPresent(Double.self, forKey: .latitude) ?? 0.0
        let longitude = try container.decodeIfPresent(Double.self, forKey: .longitude) ?? 0.0
        self.init(latitude: latitude, longitude: longitude)
    }
}

extension UIImage {
    
    func encodeImage(image: UIImage) -> Data? {
        if let data = image.jpegData(compressionQuality: 1) {
            return data
        } else {
            return nil
        }
    }
    
    func decodeImage(data: Data) -> UIImage? {
        if let image = UIImage(data: data) {
            return image
        } else {
            return nil
        }
    }
}
