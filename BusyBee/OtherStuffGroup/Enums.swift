import Foundation

enum Keys: String {
    case verificationId = "verificationId"
    case loggedIn = "loggedIn"
    case phoneNumber = "phoneNumber"
}

enum UserProperties: String {
    case telephoneNumber = "telephoneNumber"
    case uid = "uid"
    case name = "name"
    case age = "age"
    case rating = "rating"
    case tasksToComplete = "tasksToComplete"
}

enum TaskProperties: String {
    case uid = "uid"
    case employer = "employer"
    case price = "price"
    case name = "name"
    case description = "description"
    case image = "image"
    case address = "address"
    case category = "category"
    case worker = "worker"
    case candidates = "candidates"
    case addressDescription = "addressDescription"
}

