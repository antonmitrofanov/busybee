import Foundation

class Manager {
    
    static let shared = Manager()
    
    private init(){}
    
    let categories = ["household",
                      "beauty",
                      "nurse",
                      "delivery",
                      "clothes",
                      "garden",
                      "cooking",
                      "photo",
                      "animals",
                      "other"]
    
}
