import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var phoneNumberButton: UIButton!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet var labelCollection: [UILabel]!
    
    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupProfileController()
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func phoneNumberButtonPressed(_ sender: UIButton) {
        guard let user = self.user else {
            return
        }
        guard let phoneNumber = user.getTelephoneNumber() else {
            return
        }
        guard let phoneURL = URL(string: "telprompt://\(phoneNumber)") else {
            return
        }
        UIApplication.shared.open(phoneURL)
    }
    
    func setupProfileController() {
        self.setupController(viewsForGradient: [self.view], buttonsCollection: nil, viewsCollection: nil, textFieldsCollection: nil, color: nil)
        self.makeButtonsRound(buttons: [self.cancelButton])
        self.phoneNumberButton.setTitleColor(ColorManager.shared.darkBlue, for: .normal)
        for label in self.labelCollection {
            label.textColor = .white
        }
        if let user = self.user {
            if let name = user.getName() {
                self.nameLabel.text = name
            }
            
            if let age = user.getAge() {
                self.ageLabel.text = String(age)
            }
            
            if let phoneNumber = user.getTelephoneNumber() {
                self.phoneNumberButton.setTitle(phoneNumber, for: .normal)
            }
            
            if let rating = user.getRating() {
                self.ratingLabel.text = String(rating)
            }
        }
    }
    
}
