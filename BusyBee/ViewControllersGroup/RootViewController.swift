import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import CodableFirebase

class RootViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let defaults = UserDefaults.standard

    override func viewWillAppear(_ animated: Bool) {
        
        setupController(viewsForGradient: [self.view], buttonsCollection: nil, viewsCollection: nil, textFieldsCollection: nil, color: nil)
        self.activityIndicator.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)) {
            if let loggedIn = self.defaults.value(forKey: Keys.loggedIn.rawValue) as? Bool {
                if loggedIn {
                    if let phoneNumber = self.defaults.value(forKey: Keys.phoneNumber.rawValue) as? String {
                        let ref = Database.database().reference().child("users")
                        ref.observeSingleEvent(of: .value, with:{ (snapshot) in
                            if snapshot.hasChild(phoneNumber) {
                                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController  {
                                    ref.child(phoneNumber).observeSingleEvent(of: .value, with: { (userSnapshot) in
                                        guard let value = userSnapshot.value else {
                                            return
                                        }
                                        guard let user = try? FirebaseDecoder().decode(User.self, from: value) else {
                                            return
                                        }
                                        controller.user = user
                                        self.navigationController?.pushViewController(controller, animated: true)
                                    })
                                } else {
                                    guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else {
                                        return
                                    }
                                    self.activityIndicator.stopAnimating()
                                    self.navigationController?.pushViewController(controller, animated: false)
                                }
                            } else {
                                self.alertUserIsNotSignedIn()
                            }
                        })
                    } else {
                        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else {
                            return
                        }
                        self.activityIndicator.stopAnimating()
                        self.navigationController?.pushViewController(controller, animated: false)
                    }
                } else {
                    guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else {
                        return
                    }
                    self.activityIndicator.stopAnimating()
                    self.navigationController?.pushViewController(controller, animated: false)
                }
            } else {
                guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else {
                    return
                }
                self.activityIndicator.stopAnimating()
                self.navigationController?.pushViewController(controller, animated: false)
            }
        }
        
    }
    
    func alertUserIsNotSignedIn() {
        let alert = UIAlertController(title: "Ваш аккаунт был удален", message: nil, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Ок", style: .default) { (_) in
            guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else {
                return
            }
            self.activityIndicator.stopAnimating()
            self.navigationController?.pushViewController(controller, animated: false)
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }

}
