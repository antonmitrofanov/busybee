import UIKit

class CandidatesViewController: UIViewController {
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var task: Task?
    var candidates: [User]?
    
    override func viewDidLoad() {
        if let task = self.task {
            if let candidates = task.getCandidates() {
                self.candidates = candidates
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.setupCandidatesViewController()
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupCandidatesViewController() {
        self.setupController(viewsForGradient: [self.view], buttonsCollection: nil, viewsCollection: nil, textFieldsCollection: nil, color: nil)
        self.makeButtonsRound(buttons: [self.cancelButton])
    }

}

extension CandidatesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let candidates = self.candidates {
            return candidates.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CandidatesTableViewCell", for: indexPath) as? CandidatesTableViewCell else {
            return UITableViewCell()
        }
        if let candidates = self.candidates {
            if let task = self.task {
                cell.setupCell(candidate: candidates[indexPath.row], task: task, controller: self)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}
