import UIKit
import Firebase
import CodableFirebase

class CandidatesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var phoneNumberButton: UIButton!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet var labelCollection: [UILabel]!
    
    var candidate: User?
    var task: Task?
    var controller: CandidatesViewController?
    
    @IBAction func acceptButtonPressed(_ sender: UIButton) {
        self.showAlert()
    }
    
    @IBAction func phoneNumberButtonPressed(_ sender: UIButton) {
        guard let candidate = self.candidate else {
            return
        }
        guard let phoneNumber = candidate.getTelephoneNumber() else {
            return
        }
        guard let phoneURL = URL(string: "telprompt://\(phoneNumber)") else {
            return
        }
        UIApplication.shared.open(phoneURL)
    }
    
    func showAlert() {
        let alertController = UIAlertController(title: "Внимание", message: "Вы выбрали кандидата на выполнение задания", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Отменить", style: .cancel) { (_) in
            //nothing
        }
        let okAction = UIAlertAction(title: "Ок", style: .default) { (_) in
            if let task = self.task {
                if let candidate = self.candidate {
                    if let employer = task.getEmployer() {
                        if let number = employer.getTelephoneNumber() {
                            if let uid = task.getUid() {
                                task.setWorker(candidate: candidate)
                                if let data = try? FirebaseEncoder().encode(task) {
                                    Database.database().reference().child("tasks").child(number).child(uid).removeValue()
                                    Database.database().reference().child("tasks").child(number).child(uid).setValue(data)
                                    if let controller = self.controller {
                                        controller.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        if let controller = self.controller {
            controller.present(alertController, animated: true)
        }
    }
    
    func setupCell(candidate: User, task: Task, controller: CandidatesViewController) {
        
        self.acceptButton.backgroundColor = .clear
        self.acceptButton.layer.cornerRadius = 20
        self.acceptButton.layer.borderColor = UIColor.white.cgColor
        self.acceptButton.layer.borderWidth = 2
        self.acceptButton.setTitleColor(.white, for: .normal)
        
        self.phoneNumberButton.setTitleColor(ColorManager.shared.darkBlue, for: .normal)
        
        for label in self.labelCollection {
            label.textColor = .white
        }
        
        self.candidate = candidate
        self.task = task
        self.controller = controller
        
        if let name = candidate.getName() {
            self.nameLabel.text = name
        }
        
        if let age = candidate.getAge() {
            self.ageLabel.text = String(age)
        }
        
        if let phoneNumber = candidate.getTelephoneNumber() {
            self.phoneNumberButton.setTitle(phoneNumber, for: .normal)
        }
        
        if let rating = candidate.getRating() {
            self.ratingLabel.text = String(rating)
        }
    }
    
}
