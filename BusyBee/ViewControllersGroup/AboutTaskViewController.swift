import UIKit
import Firebase
import CodableFirebase

class AboutTaskViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var profileAgeLabel: UILabel!
    @IBOutlet weak var profilePhoneNumberButton: UIButton!
    @IBOutlet weak var profileRatingLabel: UILabel!
    @IBOutlet weak var goToProfileButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var sendRequestButton: UIButton!
    @IBOutlet var labelCollection: [UILabel]!
    @IBOutlet var buttonCollection: [UIButton]!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var profileView: UIView!
    
    var task: Task?
    var user: User?
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupAboutTaskViewController()
    }

    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func goToProfileButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func sendRequestButtonPressed(_ sender: UIButton) {
        guard let user = self.user else {
            return
        }
        guard let userPhoneNumber = user.getTelephoneNumber() else {
            return
        }
        guard let task = self.task else {
            return
        }
        guard let employer = task.getEmployer() else {
            return
        }
        guard let phoneNumber = employer.getTelephoneNumber() else {
            return
        }
        guard let uid = task.getUid() else {
            return
        }
        
        if let candidates = task.getCandidates() {
            if candidates.contains(user) {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        task.appendCandidates(user: user)
        guard let data = try? FirebaseEncoder().encode(task) else {
            return
        }
        
        Database.database().reference().child("tasks").child(phoneNumber).child(uid).removeValue()
        Database.database().reference().child("tasks").child(phoneNumber).child(uid).setValue(data)
        
        let textTask = TextTask(telephoneNumber: phoneNumber, uid: uid)
        
        user.appendTasksToComplete(task: textTask)
        guard let userData = try? FirebaseEncoder().encode(user) else {
            return
        }
        
        Database.database().reference().child("users").child(userPhoneNumber).removeValue()
        Database.database().reference().child("users").child(userPhoneNumber).setValue(userData)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func profilePhoneNumberButtonPressed(_ sender: UIButton) {
        guard let task = self.task else {
            return
        }
        guard let employer = task.getEmployer() else {
            return
        }
        guard let phoneNumber = employer.getTelephoneNumber() else {
            return
        }
        guard let phoneURL = URL(string: "telprompt://\(phoneNumber)") else {
            return
        }
        UIApplication.shared.open(phoneURL)
    }
    
    func setupAboutTaskViewController() {
        self.setupController(viewsForGradient: [self.view], buttonsCollection: self.buttonCollection, viewsCollection: nil, textFieldsCollection: nil, color: nil)
        self.profileView.layer.borderColor = UIColor.white.cgColor
        self.profileView.layer.borderWidth = 2
        
        for label in self.labelCollection {
            label.textColor = .white
        }
        
        self.profilePhoneNumberButton.setTitleColor(ColorManager.shared.darkBlue, for: .normal)
        self.descriptionTextView.layer.cornerRadius = 20
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.width/2
        self.priceView.layer.cornerRadius = 20
        self.makeButtonsRound(buttons: [self.cancelButton])
        
        guard let task = self.task else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        if let employer = task.getEmployer() {
            if let name = employer.getName() {
                self.profileNameLabel.text = name
            }
            if let age = employer.getAge() {
                self.profileAgeLabel.text = String(age)
            }
            if let telephoneNumber = employer.getTelephoneNumber() {
                self.profilePhoneNumberButton.setTitle(telephoneNumber, for: .normal)
            }
            if let rating = employer.getRating() {
                self.profileRatingLabel.text = String(rating)
            }
        }
        
        if let category = task.getCategory() {
            self.categoryLabel.text = category
        }
        
        if let name = task.getName() {
            self.nameLabel.text = name
        }
        
        if let description = task.getDescription() {
            self.descriptionTextView.text = description
        }
        
        // if let image = task.getImage
        
        if let addressDescription = task.getAddressDescription() {
            self.addressLabel.text = addressDescription
        }
        
        if let price = task.getPrice() {
            self.priceLabel.text = String(price) + " BYN"
        }
        
    }
    
}
