import UIKit

protocol CategoriesViewControllerDelegate {
    func setCategory(category: String)
}

class CategoriesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navBar: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    
    var delegate: CategoriesViewControllerDelegate?
    var category: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupCategoriesViewController()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let delegate = self.delegate {
            if let category = self.category {
                delegate.setCategory(category: category)
            }
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupCategoriesViewController() {
        self.setupController(viewsForGradient: [self.view], buttonsCollection: nil, viewsCollection: nil, textFieldsCollection: nil, color: nil)
        self.makeButtonsRound(buttons: [self.cancelButton])
    }
    
}

extension CategoriesViewController: UITableViewDelegate, UITableViewDataSource {
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 55
        }
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return Manager.shared.categories.count
            //return 0
        }
    
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesTableViewCell", for: indexPath) as? CategoriesTableViewCell else {
                return UITableViewCell()
            }
            cell.tintColor = .white
            cell.label.text = Manager.shared.categories[indexPath.row]
            cell.label.textColor = .white
            return cell
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        for cell in tableView.visibleCells {
            cell.accessoryType = UITableViewCell.AccessoryType.none
        }
        tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCell.AccessoryType.checkmark
        self.category = Manager.shared.categories[indexPath.row]
    }
}


