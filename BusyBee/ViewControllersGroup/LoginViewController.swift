import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import CodableFirebase


class LoginViewController: UIViewController {

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var verifyWindow: UIView!
    @IBOutlet weak var verificationCodeTextField: UITextField!
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet var buttonsCollection: [UIButton]!
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.coverView.isHidden = true
        self.setupController(viewsForGradient: [self.view, self.coverView], buttonsCollection: self.buttonsCollection, viewsCollection: [self.verifyWindow], textFieldsCollection: [phoneNumberTextField, verificationCodeTextField], color: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.first != nil {
            view.endEditing(true)
        }
        super.touchesBegan(touches, with: event)
    }

    @IBAction func loginButtonPressed(_ sender: UIButton) {
        if let phoneNumber = self.phoneNumberTextField.text {
            PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationId, error) in
                if let error = error {
                    print("Unable to get Secret Verification Code from Firebase", error.localizedDescription)
                } else {
                    if let verificationId = verificationId {
                        self.coverView.isHidden = false
                        self.defaults.set(verificationId, forKey: Keys.verificationId.rawValue)
                        print(verificationId)
                    }
                }
            }
        }
    }
    
    @IBAction func verifyButtonPressed(_ sender: UIButton) {
        if let text = self.verificationCodeTextField.text {
            
            guard let verificationId = self.defaults.value(forKey: Keys.verificationId.rawValue) as? String else {
                return
            }
            
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationId, verificationCode: text)
            
            Auth.auth().signInAndRetrieveData(with: credential) { (success, error) in
                if let error = error {
                    print("Something went wrong: ", error.localizedDescription)
                } else {
                    if let success = success {
                        print(success)
                        print("User signed in successfully")
                        self.defaults.set(true, forKey: Keys.loggedIn.rawValue)
                        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController else {
                            return
                        }
                        if let currentUser = Auth.auth().currentUser {
                            if let phoneNumber = currentUser.phoneNumber {
                                self.defaults.set(phoneNumber, forKey: Keys.phoneNumber.rawValue)
                                let uid = currentUser.uid
                                let values = [UserProperties.telephoneNumber.rawValue : phoneNumber, UserProperties.uid.rawValue : uid]
                                let user = User(values: values)
                                guard let data = try? FirebaseEncoder().encode(user) else {
                                    return
                                }
                                Database.database().reference().child("users").child(phoneNumber).setValue(data)
                                controller.user = user
                                self.navigationController?.pushViewController(controller, animated: true)
                            }
                        }
                    }
                }
            }
        }
    }
    
}

