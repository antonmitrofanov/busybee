import MapKit
import UIKit
import CoreLocation

protocol SetTaskLocationViewControllerDelegate {
    func setTaskLocation(location: CLLocation, placeName: String)
}

class SetTaskLocationViewController: UIViewController {
    
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var pin: UIImageView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var myLocationButton: UIButton!
    @IBOutlet var oneTypeButtons: [UIButton]!
    
    let locationManager = CLLocationManager()
    let defaultZoom: Double = 100
    var previousLocation: CLLocation?
    var placeNameForDelegate: String?
    var resultSearchController: UISearchController? = nil
    var delegate: SetTaskLocationViewControllerDelegate?
    
    //MARK: - override funcs
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupTaskLocationController()
        self.checkLocationServices()
        self.createTaskLocationAlert()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let previousLocation = self.previousLocation {
            if let placeName = self.placeNameForDelegate {
                if let delegate = self.delegate {
                    delegate.setTaskLocation(location: previousLocation, placeName: placeName)
                }
            }
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func confirmButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func myLocationButtonPressed(_ sender: UIButton) {
        self.centerViewOnUserLocation()
    }
    
    //MARK: - UX setups
    
    func setupTaskLocationController() {
        self.addressLabel.textColor = ColorManager.shared.darkBlue
        self.makeButtonsRound(buttons: self.oneTypeButtons)
    }
    
    
    
    //MARK: - location
    
    func setupLocationManager() {
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.centerViewOnUserLocation()
        self.locationManager.startUpdatingLocation()
        self.previousLocation = self.getCenterLocation(for: self.mapView)
    }
    
    func checkLocationServices() {
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse && CLLocationManager.authorizationStatus() != .authorizedAlways {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.setupLocationManager()
        }
    }
    
    func centerViewOnUserLocation() {
        if let location = self.locationManager.location {
            let region = MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: self.defaultZoom, longitudinalMeters: self.defaultZoom)
            self.mapView.setRegion(region, animated: true)
        }
    }
    
    func centerViewOnLocation(location: CLLocationCoordinate2D) {
        let region = MKCoordinateRegion.init(center: location, latitudinalMeters: self.defaultZoom, longitudinalMeters: self.defaultZoom)
        self.mapView.setRegion(region, animated: true)
    }
    
    func getCenterLocation(for mapView: MKMapView) -> CLLocation {
        let latitude = mapView.centerCoordinate.latitude
        let longitude = mapView.centerCoordinate.longitude
        return CLLocation(latitude: latitude, longitude: longitude)
    }
    
    func createTaskLocationAlert() {
        let alertController = UIAlertController(title: "Укажите точное местоположение", message: "Укажите метоположение вашего задания так, как его будут видеть другие пользователи", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Отменить", style: .destructive) { (_) in
            self.navigationController?.popViewController(animated: true)
        }
        let okAction = UIAlertAction(title: "Ок", style: .default) { (_) in
            //nothing
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true)
    }
    
    
}

//MARK: -  extensions

extension SetTaskLocationViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse && CLLocationManager.authorizationStatus() != .authorizedAlways {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension SetTaskLocationViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let center = self.getCenterLocation(for: mapView)
        let geoCoder = CLGeocoder()
        guard let previousLocation = self.previousLocation else {
            return
        }
        guard center.distance(from: previousLocation) > 50 else {
            return
        }
        
        self.previousLocation = center
        
        geoCoder.reverseGeocodeLocation(center) { [weak self] (placemarks, error) in
            guard let self = self else {
                return
            }
            
            if let _ = error {
                //alert
                return
            }
            
            guard let placemark = placemarks?.first else {
                //alert
                return
            }
            
            let placeName = placemark.name ?? ""

            
            self.placeNameForDelegate = placeName
            
            DispatchQueue.main.async {
                self.addressLabel.text = "\(placeName)"
            }
            
        }
    }
}
