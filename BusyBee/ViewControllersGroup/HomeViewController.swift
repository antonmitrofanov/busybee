import UIKit
import MapKit
import CoreLocation
import Firebase
import FirebaseDatabase
import CodableFirebase

class HomeViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var addTaskButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var tasksButton: UIButton!
    @IBOutlet weak var myLocationButton: UIButton!
    @IBOutlet weak var categoriesButton: UIButton!
    @IBOutlet var oneTypeButtons: [UIButton]!
    
    var user: User?
    var coverView: UIView?
    
    let locationManager = CLLocationManager()
    let defaultZoom: Double = 1000
    var pinTaskView: UIView?
    var tapGestureRecognizer: UITapGestureRecognizer?
    var categories: [String]?
    var tasks = [Task]()
    var annotations = [CustomAnnotation]()
    
    //MARK: - override funcs
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let coverView = self.coverView {
            coverView.removeFromSuperview()
        }
    
        self.checkLocationServices()
        self.setupHomeController()
        self.readTasksFromFirebase()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.removeTasksObserver()
    }
    
    //MARK: - IBActions
    
    @IBAction func addTaskButtonPressed(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddTaskViewController") as? AddTaskViewController else {
            return
        }
        if let user = self.user {
            controller.employer = user
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func tasksButtonPressed(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "MyTasksViewController") as? MyTasksViewController else {
            return
        }
        var tasksForController = [Task]()
        guard let user = self.user else {
            return
        }
        
        for task in self.tasks {
            if let employer = task.getEmployer() {
                if employer == user {
                    tasksForController.append(task)
                }
            }
        }
        
        controller.tasks = tasksForController
        
        if let textTasks = user.getTasksToComplete() {
            self.createCoverView()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)) {
                self.observeTasksToComplete(textTasks: textTasks) { (result, error) in
                    if let error = error {
                        print(error.localizedDescription)
                    } else if let result = result {
                        controller.tasksToComplete = result
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                }
            }
        } else {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func profileButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func myLocationButtonPressed(_ sender: UIButton) {
        self.centerViewOnUserLocation()
    }
    
    @IBAction func categoriesButtonPressed(_ sender: UIButton) {
    }
    
    //MARK: - UX setups
    
    func createCoverView() {
        let coverView = UIView()
        coverView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        coverView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
        let activityIndicator = UIActivityIndicatorView()
        let widthHeight: CGFloat = 50
        let size = CGSize(width: widthHeight, height: widthHeight)
        let center = self.view.center
        activityIndicator.frame.size = size
        activityIndicator.center = center
        activityIndicator.color = ColorManager.shared.darkBlue
        coverView.addSubview(activityIndicator)
        self.view.addSubview(coverView)
        self.coverView = coverView
        activityIndicator.startAnimating()
    }
    
    func setupHomeController() {
        self.myLocationButton.layer.cornerRadius = self.myLocationButton.frame.width/2
        self.myLocationButton.backgroundColor = ColorManager.shared.whiteColorForElements
        self.makeButtonsRound(buttons: self.oneTypeButtons)
    }
    
    func buttonsAreHidden(input: Bool, buttons: [UIButton]) {
        if input {
            for button in buttons {
                UIView.animate(withDuration: 0.5, animations: {
                    button.alpha = 0
                }) { (_) in
                    button.isHidden = true
                }
            }
        } else {
            for button in buttons {
                button.isHidden = false
                UIView.animate(withDuration: 0.5) {
                    button.alpha = 1
                }
            }
        }
    }
    
    //MARK: - location
    
    func setupLocationManager() {
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.centerViewOnUserLocation()
        self.locationManager.startUpdatingLocation()
    }
    
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .authorizedWhenInUse, .authorizedAlways:
                self.setupLocationManager()
                break
            case .denied, .restricted:
                self.alertLocationSettings()
                break
            case .notDetermined:
                self.locationManager.requestWhenInUseAuthorization()
                break
            }
        } else {
            self.alertLocationSettings()
        }
    }
    
    func centerViewOnUserLocation() {
        if let location = self.locationManager.location {
            let region = MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: self.defaultZoom, longitudinalMeters: self.defaultZoom)
            self.mapView.setRegion(region, animated: true)
        }
    }
    
    func alertLocationSettings() {
        let alert = UIAlertController(title: "Карты не знают где вы находитесь.", message: "Разрешите доступ к геолокации в настройках.", preferredStyle: UIAlertController.Style.alert)
        
        let settingAction = UIAlertAction(title: "Настройки", style: UIAlertAction.Style.default) { (_)  -> Void in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!) }
        
        alert.addAction(settingAction)
        
        alert.addAction(UIAlertAction(title: "Отменить", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - annotations
    
    func readTasksFromFirebase() {
        DispatchQueue.main.async {
            let ref = Database.database().reference().child("tasks")
            ref.observe(.value, with: { (telephoneNumbersSnapshot) in
                if !self.tasks.isEmpty {
                    self.mapView.removeAnnotations(self.annotations)
                    self.tasks = [Task]()
                    self.annotations = [CustomAnnotation]()
                }
                if let telephoneNumbers = telephoneNumbersSnapshot.children.allObjects as? [DataSnapshot] {
                    for telephoneNumber in telephoneNumbers {
                        if let uids = telephoneNumber.children.allObjects as? [DataSnapshot] {
                            for uid in uids {
                                if let encodedTask = uid.value {
                                    if let task = try? FirebaseDecoder().decode(Task.self, from: encodedTask) {
                                        if let imageURL = task.getImageURL() {
                                            if let url = URL(string: imageURL) {
                                                URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                                                    if let error = error {
                                                        print(error.localizedDescription)
                                                    } else {
                                                        if let data = data {
                                                            if let image = UIImage(data: data) {
                                                                task.setImage(image: image)
                                                            }
                                                        }
                                                    }
                                                })
                                            }
                                        }
                                        self.tasks.append(task)
                                    }
                                }
                            }
                        }
                    }
                }
                self.createAnnotations(categories: self.categories)
            })
        }
    }
    
    func createAnnotations(categories: [String]?) {
        if let categories = categories {
            //do smth
        } else {
            for task in self.tasks {
                if let address = task.getAddress() {
                    let annotation = CustomAnnotation(coordinate: address, title: nil, subtitle: nil, task: task)
                    self.annotations.append(annotation)
                }
            }
            self.mapView.addAnnotations(self.annotations)
        }
    }
    
    func removeTasksObserver() {
        let ref = Database.database().reference().child("tasks")
        ref.removeAllObservers()
    }
    
    //MARK: - observing new tasks from database
    
    func observeTasksToComplete(textTasks: [TextTask], completion: @escaping (_ result: [Task]?, _ error: Error?) -> ()) {
        var resultArray = [Task]()
        for textTask in textTasks {
            if let number = textTask.getTelephoneNumber() {
                if let uid = textTask.getUid() {
                    Database.database().reference().child("tasks").child(number).child(uid).observeSingleEvent(of: .value) { (snapshot) in
                        if let data = snapshot.value {
                            if let task = try? FirebaseDecoder().decode(Task.self, from: data) {
                                resultArray.append(task)
                            }
                            if textTask == textTasks[textTasks.endIndex-1] {
                                completion(resultArray, nil)
                            }
                        }
                    }
                }
            }
        }
    }

}

//MARK: - extensions

extension HomeViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        guard let location = locations.last else {
//            return
//        }
//        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
//        let region = MKCoordinateRegion.init(center: center, latitudinalMeters: self.defaultZoom, longitudinalMeters: self.defaultZoom)
//        self.mapView.setRegion(region, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.checkLocationServices()
    }
    
}

extension HomeViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation.isKind(of: MKUserLocation.self) {
            return nil
        } else {
            let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: nil)
            guard let image = UIImage(named: "pinMini") else {
                return nil
            }
            let size = CGSize(width: 50, height: 50)
            UIGraphicsBeginImageContext(size)
            image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            annotationView.image = resizedImage
            //annotationView.image = image
            return annotationView
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let annotation = view.annotation as? CustomAnnotation {
            if self.annotations.contains(annotation) {
                let annotations = self.annotations as NSArray
                let task = self.tasks[annotations.index(of: annotation)]
                self.createPinTaskView(task: task)
            } else {
                view.isSelected = false
            }
        } else {
            view.isSelected = false
        }
    }
    
    func createPinTaskView(task: Task) {
        let pinTaskView: PinTaskView = PinTaskView.instanceFromNib() as! PinTaskView
        let width: CGFloat = 0.7 * self.view.frame.width
        let height: CGFloat = 0.3 * self.view.frame.height
        pinTaskView.frame = CGRect(x: self.view.frame.width/2 - width/2, y: self.view.frame.height/2 - height/2, width: width, height: height)
        pinTaskView.layer.cornerRadius = 20
        if let user = self.user {
            pinTaskView.setupView(task: task, controller: self, user: user)
        }
        pinTaskView.alpha = 0
        DispatchQueue.main.async {
            self.view.addSubview(pinTaskView)
            UIView.animate(withDuration: 0.5, animations: {
                pinTaskView.alpha = 1
            })
            self.createPinTaskViewRecognizer()
            self.buttonsAreHidden(input: true, buttons: self.oneTypeButtons)
        }
        self.pinTaskView = pinTaskView
    }
    
    func createPinTaskViewRecognizer() {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureRecognized(_:)))
        self.tapGestureRecognizer = recognizer
        self.mapView.addGestureRecognizer(recognizer)
    }
    
    @IBAction func tapGestureRecognized(_ sender: UITapGestureRecognizer) {
        if let view = self.pinTaskView {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.5, animations: {
                    view.alpha = 0
                }, completion: { (_) in
                    view.removeFromSuperview()
                })
                self.buttonsAreHidden(input: false, buttons: self.oneTypeButtons)
            }
            self.pinTaskView = nil
            if let recognizer = self.tapGestureRecognizer {
                self.mapView.removeGestureRecognizer(recognizer)
                self.tapGestureRecognizer = nil
            }
        }
    }
    
}
