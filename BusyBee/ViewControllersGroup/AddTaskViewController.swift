import UIKit
import CoreLocation
import CodableFirebase
import Firebase
import FirebaseDatabase
import FirebaseStorage

class AddTaskViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var newTaskLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var selectPhotoButton: UIButton!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryButton: UIButton!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var addTaskButton: UIButton!
    @IBOutlet var rectButtons: [UIButton]!
    @IBOutlet var roundButtons: [UIButton]!
    @IBOutlet var textFields: [UITextField]!
    
    var employer: User?
    
    let imagePicker = UIImagePickerController()
    var location: CLLocation?
    var placeName: String?
    var category: String?
    var image: UIImage?
    var controllerWasShown = false
    
    //MARK: - override funcs
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !controllerWasShown {
            self.setupAddTaskViewController()
            self.scrollView.keyboardDismissMode = .onDrag
            self.registerForKeyboardNotifications()
            self.controllerWasShown = true
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func locationButtonPressed(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "SetTaskLocationViewController") as? SetTaskLocationViewController else {
            return
        }
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func categoryButtonPressed(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "CategoriesViewController") as? CategoriesViewController else {
            return
        }
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func selectPhotoButtonPressed(_ sender: UIButton) {
        self.createImagePickerAlert()
    }
    
    @IBAction func addTaskButtonPressed(_ sender: UIButton) {
        guard let employer = self.employer else {
            return
        }
        guard let name = self.nameTextField.text else {
            return
        }
        if name != "" {
            guard let description = self.descriptionTextView.text else {
                return
            }
            if description != "" {
                guard let category = self.category else {
                    return
                }
                guard let location = self.location else {
                    return
                }
                guard let placeName = self.placeName else {
                    return
                }
                guard let priceString = self.priceTextField.text else {
                    return
                }
                guard let price = Double(priceString) else {
                    return
                }
                let locationCoordinate = location.coordinate
                let values = [TaskProperties.employer.rawValue : employer,
                              TaskProperties.name.rawValue : name,
                              TaskProperties.description.rawValue : description,
                              TaskProperties.address.rawValue : locationCoordinate,
                              TaskProperties.category.rawValue : category,
                              TaskProperties.price.rawValue : price,
                              TaskProperties.addressDescription.rawValue : placeName] as [String : Any]
                self.createCoverView()
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)) {
                    self.createTask(values: values)
                }
            }
        }
    }
    
    func createCoverView() {
        let coverView = UIView()
        coverView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        coverView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
        let activityIndicator = UIActivityIndicatorView()
        let widthHeight: CGFloat = 50
        let size = CGSize(width: widthHeight, height: widthHeight)
        let center = self.view.center
        activityIndicator.frame.size = size
        activityIndicator.center = center
        activityIndicator.color = ColorManager.shared.darkBlue
        coverView.addSubview(activityIndicator)
        self.view.addSubview(coverView)
        activityIndicator.startAnimating()
    }
    
    //MARK: - imagePicker funcs
    
    func createImagePickerAlert() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Камера", style: .default) { (_) in
            self.presentImagePickerController(sourceType: .camera)
        }
        let photoLibraryAction = UIAlertAction(title: "Галерея", style: .default) { (_) in
            self.presentImagePickerController(sourceType: .photoLibrary)
        }
        let cancelAction = UIAlertAction(title: "Отменить", style: .cancel) { (_) in
            //do nothing
        }
        alertController.addAction(cameraAction)
        alertController.addAction(photoLibraryAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true)
    }
    
    func presentImagePickerController(sourceType: UIImagePickerController.SourceType) {
        self.imagePicker.sourceType = sourceType
        present(self.imagePicker, animated: true, completion: nil)
    }
    
    //MARK: - UX setups
    
    func setupAddTaskViewController() {
        self.setupController(viewsForGradient: [self.view], buttonsCollection: self.rectButtons, viewsCollection: nil, textFieldsCollection: self.textFields, color: nil)
        self.makeButtonsRound(buttons: self.roundButtons)
        self.descriptionTextView.layer.cornerRadius = 20
        self.categoryLabel.textColor = ColorManager.shared.pinkWarning
        self.locationLabel.textColor = ColorManager.shared.pinkWarning
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    
    @objc private func keyboardWillShow(_ notification: NSNotification) {
        let userInfo = notification.userInfo!
        let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            self.scrollView.contentInset.bottom = 0
        } else {
            self.scrollView.contentInset.bottom = keyboardScreenEndFrame.height + 50
        }
        
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK: - working with tasks and database
    
    func createTask(values: [String : Any]) {
        
        let task = Task(values: values)
        guard let employer = task.getEmployer() else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        guard let telephoneNumber = employer.getTelephoneNumber() else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        guard let uid = task.getUid() else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        if let image = self.image {
            if let imageData = image.jpegData(compressionQuality: 1) {
                let storageRef = Storage.storage().reference().child("tasks").child(telephoneNumber).child(uid)
                storageRef.putData(imageData, metadata: nil) { (metadata, error) in
                    if let error = error {
                        print(error.localizedDescription)
                        guard let data = try? FirebaseEncoder().encode(task) else {
                            return
                        }
                        Database.database().reference().child("tasks").child(telephoneNumber).child(uid).setValue(data)
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        if let _ = metadata {
                            storageRef.downloadURL(completion: { (url, error) in
                                if let error = error {
                                    print(error.localizedDescription)
                                    guard let data = try? FirebaseEncoder().encode(task) else {
                                        return
                                    }
                                    Database.database().reference().child("tasks").child(telephoneNumber).child(uid).setValue(data)
                                    self.navigationController?.popViewController(animated: true)
                                } else {
                                    if let url = url {
                                        task.setImageURL(url: url.absoluteString)
                                        guard let data = try? FirebaseEncoder().encode(task) else {
                                            return
                                        }
                                        Database.database().reference().child("tasks").child(telephoneNumber).child(uid).setValue(data)
                                        self.navigationController?.popViewController(animated: true)
                                    } else {
                                        guard let data = try? FirebaseEncoder().encode(task) else {
                                            return
                                        }
                                        Database.database().reference().child("tasks").child(telephoneNumber).child(uid).setValue(data)
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            })
                        } else {
                            guard let data = try? FirebaseEncoder().encode(task) else {
                                return
                            }
                            Database.database().reference().child("tasks").child(telephoneNumber).child(uid).setValue(data)
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
        } else {
            guard let data = try? FirebaseEncoder().encode(task) else {
                return
            }
            Database.database().reference().child("tasks").child(telephoneNumber).child(uid).setValue(data)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}

extension AddTaskViewController: SetTaskLocationViewControllerDelegate {
    func setTaskLocation(location: CLLocation, placeName: String) {
        self.location = location
        self.placeName = placeName
        self.locationLabel.text = placeName
        self.locationLabel.textColor = ColorManager.shared.greenAccepting
        self.locationButton.setTitle("Изменить месторасположение", for: .normal)
    }
    
}

extension AddTaskViewController: CategoriesViewControllerDelegate {
    func setCategory(category: String) {
        self.category = category
        self.categoryLabel.text = category
        self.categoryLabel.textColor = ColorManager.shared.greenAccepting
        self.categoryButton.setTitle("Изменить категорию", for: .normal)
    }
}

extension AddTaskViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.imageView.image = pickedImage
            self.image = pickedImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}


