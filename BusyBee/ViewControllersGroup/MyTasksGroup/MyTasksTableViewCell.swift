import UIKit

class MyTasksTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    var user: User?
    
    func setupCell(task: Task, isWorker: Bool) {
        self.nameLabel.textColor = ColorManager.shared.darkBlue
        if let name = task.getName() {
            self.nameLabel.text = name
        }
        if let worker = task.getWorker() {
            self.statusLabel.text = "Утверждено"
            self.statusLabel.textColor = ColorManager.shared.greenAccepting
            if let user = self.user {
                if isWorker && worker != user {
                    self.statusLabel.textColor = ColorManager.shared.pinkWarning
                    self.statusLabel.text = "Не принята"
                } else if isWorker && worker == user {
                    self.statusLabel.textColor = ColorManager.shared.pinkWarning
                    self.statusLabel.text = "Не принята"
                }
            }
        } else {
            self.statusLabel.text = "Ожидание"
            self.statusLabel.textColor = .white
        }
    }
    
}
