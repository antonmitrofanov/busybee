import UIKit

class MyTasksViewController: UIViewController {
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var segmentedControll: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    var tasks: [Task]?
    var tasksToComplete: [Task]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupMyTasksViewController()
    }
    
    @IBAction func segmentControllValueChanged(_ sender: UISegmentedControl) {
        self.tableView.reloadData()
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupMyTasksViewController() {
        self.setupController(viewsForGradient: [self.view], buttonsCollection: nil, viewsCollection: nil, textFieldsCollection: nil, color: nil)
        self.makeButtonsRound(buttons: [self.cancelButton])
    }

}

extension MyTasksViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.segmentedControll.selectedSegmentIndex == 0 {
            if let tasks = self.tasks {
                return tasks.count
            } else {
                return 0
            }
        } else {
            if let tasksToComplete = self.tasksToComplete {
                return tasksToComplete.count
            } else {
                return 0
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MyTasksTableViewCell", for: indexPath) as? MyTasksTableViewCell else {
            return UITableViewCell()
        }
        if segmentedControll.selectedSegmentIndex == 0 {
            if let tasks = self.tasks {
                cell.setupCell(task: tasks[indexPath.row], isWorker: false)
            }
        } else {
            if let tasksToComplete = self.tasksToComplete {
                cell.setupCell(task: tasksToComplete[indexPath.row], isWorker: true)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.segmentedControll.selectedSegmentIndex == 0 {
            guard let tasks = self.tasks else {
                return
            }
            let task = tasks[indexPath.row]
            if let worker = task.getWorker() {
                guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController else {
                    return
                }
                controller.user = worker
                self.navigationController?.pushViewController(controller, animated: true)
            } else {
                guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "CandidatesViewController") as? CandidatesViewController else {
                    return
                }
                controller.task = tasks[indexPath.row]
                self.navigationController?.pushViewController(controller, animated: true)
            }
            
            
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
