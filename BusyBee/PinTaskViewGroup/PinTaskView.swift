

import UIKit

class PinTaskView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    
    var controller: HomeViewController?
    var task: Task?
    var user: User?
    
    func setupView(task: Task, controller: HomeViewController, user: User) {
        self.imageView.layer.cornerRadius = self.imageView.frame.width/2
        self.imageView.contentMode = .scaleAspectFit
        
        self.controller = controller
        self.task = task
        self.user = user

        self.backgroundColor = ColorManager.shared.whiteColorForElements
        self.layer.borderWidth = 1
        self.layer.borderColor = ColorManager.shared.darkBlue.cgColor
        self.taskLabel.textColor = ColorManager.shared.darkBlue
        self.priceLabel.textColor = ColorManager.shared.darkBlue
        self.addressLabel.textColor = ColorManager.shared.darkBlue
        self.button.backgroundColor = .clear
        self.button.layer.borderWidth = 1
        self.button.layer.borderColor = ColorManager.shared.darkBlue.cgColor
        self.button.setTitleColor(ColorManager.shared.darkBlue, for: .normal)
        self.button.layer.cornerRadius = 20
        if let employer = task.getEmployer() {
            if let image = employer.getImage() {
                self.imageView.image = image
            } else {
                self.imageView.image = #imageLiteral(resourceName: "logo")
            }
        } else {
            self.imageView.image = #imageLiteral(resourceName: "logo")
        }
        if let name = task.getName() {
            self.taskLabel.text = name
        }
        if let placeName = task.getAddressDescription() {
            self.addressLabel.text = placeName
        }
        if let price = task.getPrice() {
            self.priceLabel.text = String(price) + " BYN"
        }
    }
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        guard let controller = self.controller, let task = self.task, let user = self.user else {
            return
        }
        guard let controllerToPush = controller.storyboard?.instantiateViewController(withIdentifier: "AboutTaskViewController") as? AboutTaskViewController else {
            return
        }
        controllerToPush.task = task
        controllerToPush.user = user
        controller.navigationController?.pushViewController(controllerToPush, animated: true)
    }
    
}

extension PinTaskView {
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "PinTaskView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
