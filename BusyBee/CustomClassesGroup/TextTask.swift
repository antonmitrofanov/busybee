import Foundation

class TextTask: Codable {
    
    static func == (lhs: TextTask, rhs: TextTask) -> Bool {
        return lhs.uid == rhs.uid
    }
    
    private var telephoneNumber: String?
    private var uid: String?
    
    init(telephoneNumber: String, uid: String) {
        self.telephoneNumber = telephoneNumber
        self.uid = uid
    }
    
    func getTelephoneNumber() -> String? {
        return self.telephoneNumber ?? nil
    }
    
    func getUid() -> String? {
        return self.uid ?? nil
    }
    
}
