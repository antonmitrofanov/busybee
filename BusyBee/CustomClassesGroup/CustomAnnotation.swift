import Foundation
import MapKit

class CustomAnnotation: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var task: Task?
    
    init(coordinate: CLLocationCoordinate2D, title: String?, subtitle: String?, task: Task?) {
        self.coordinate = coordinate
        self.title = title ?? nil
        self.subtitle = subtitle ?? nil
    }
    
    
    func getTask() -> Task? {
        return self.task ?? nil
    }
}
