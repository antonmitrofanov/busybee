import Foundation
import UIKit
import CoreLocation

class Task: Codable {
    
    private var uid: String?
    private var employer: User?
    private var price: Double?
    private var name: String?
    private var description: String?
    private var imageURL: String?
    private var address: CLLocationCoordinate2D?
    private var category: String?
    private var worker: User?
    private var candidates: [User]?
    private var image: UIImage?
    private var addressDescription: String?
    
    enum CodingKeys: String, CodingKey {
        case uid, employer, price, name, description, imageURL, address, category, worker, candidates, addressDescription
    }
    
    init (values: [String : Any]) {
        self.uid = UUID().uuidString
        self.employer = values[TaskProperties.employer.rawValue] as? User
        self.price = values[TaskProperties.price.rawValue] as? Double
        self.name = values[TaskProperties.name.rawValue] as? String
        self.description = values[TaskProperties.description.rawValue] as? String
        self.imageURL = values[TaskProperties.image.rawValue] as? String
        self.address = values[TaskProperties.address.rawValue] as? CLLocationCoordinate2D
        self.category = values[TaskProperties.category.rawValue] as? String
        self.worker = values[TaskProperties.worker.rawValue] as? User
        self.candidates = values[TaskProperties.candidates.rawValue] as? [User]
        self.addressDescription = values[TaskProperties.addressDescription.rawValue] as? String
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.uid = try container.decodeIfPresent(String.self, forKey: .uid)
        self.employer  = try container.decodeIfPresent(User.self, forKey: .employer)
        self.price  = try container.decodeIfPresent(Double.self, forKey: .price)
        self.name  = try container.decodeIfPresent(String.self, forKey: .name)
        self.description  = try container.decodeIfPresent(String.self, forKey: .description)
        self.imageURL = try container.decodeIfPresent(String.self, forKey: .imageURL)
        self.address  = try container.decodeIfPresent(CLLocationCoordinate2D.self, forKey: .address)
        self.category  = try container.decodeIfPresent(String.self, forKey: .category)
        self.worker  = try container.decodeIfPresent(User.self, forKey: .worker)
        self.candidates  = try container.decodeIfPresent([User].self, forKey: .candidates)
        self.addressDescription = try container.decodeIfPresent(String.self, forKey: .addressDescription)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.uid, forKey: .uid)
        try container.encode(self.employer, forKey: .employer)
        try container.encode(self.price, forKey: .price)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.description, forKey: .description)
        try container.encode(self.imageURL, forKey: .imageURL)
        try container.encode(self.address, forKey: .address)
        try container.encode(self.category, forKey: .category)
        try container.encode(self.worker, forKey: .worker)
        try container.encode(self.candidates, forKey: .candidates)
        try container.encode(self.addressDescription, forKey: .addressDescription)
    }
    
    func setImageURL(url: String) {
        self.imageURL = url
    }
    
    func setImage(image: UIImage) {
        self.image = image
    }
    
    func setWorker(candidate: User) {
        self.worker = candidate
    }
    
    func getImageURL() -> String? {
        return self.imageURL ?? nil
    }
    
    func getUid() -> String? {
        return self.uid ?? nil
    }

    func getEmployer() -> User? {
        if let employer = self.employer {
            return employer
        } else {
            return nil
        }
    }
    
    func getPrice() -> Double? {
        return self.price ?? nil
    }
    
    func getName() -> String? {
        return self.name ?? nil
    }
    
    func getDescription() -> String? {
        return self.description ?? nil
    }
    
    func getAddress() -> CLLocationCoordinate2D? {
        return self.address ?? nil
    }
    
    func getCategory() -> String? {
        return self.category ?? nil
    }
    
    func getWorker() -> User? {
        return self.worker ?? nil
    }
    
    func getCandidates() -> [User]? {
        return self.candidates ?? nil
    }
    
    func getAddressDescription() -> String? {
        return self.addressDescription ?? nil
    }
    
    func appendCandidates(user: User) {
        if var candidates = self.candidates {
            candidates.append(user)
            self.candidates = candidates
        } else {
            let candidates = [user]
            self.candidates = candidates
        }
    }
    
}
