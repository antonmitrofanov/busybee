import Foundation
import UIKit

class User: Codable, Equatable {
    
    static func == (lhs: User, rhs: User) -> Bool {
        return lhs.uid == rhs.uid
    }
    
    
    private var telephoneNumber: String?
    private var uid: String?
    private var name: String?
    private var age: Int?
    private var image: UIImage?
    private var rating: Double?
    private var tasksToComplete: [TextTask]?
    
    enum CodingKeys: String, CodingKey {
        case telephoneNumber, uid, name, age, image, rating, tasksToComplete
    }

    
    init(values: [String : Any]) {
        self.telephoneNumber = values[UserProperties.telephoneNumber.rawValue] as? String
        self.uid = values[UserProperties.uid.rawValue] as? String
        self.name = values[UserProperties.name.rawValue] as? String
        self.age = values[UserProperties.age.rawValue] as? Int
        self.rating = values[UserProperties.rating.rawValue] as? Double
        self.tasksToComplete = values[UserProperties.tasksToComplete.rawValue] as? [TextTask]
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
//        let data = try container.decodeIfPresent(Data.self, forKey: .image)
//        if let data = data {
//            guard let image = UIImage(data: data) else {
//                return
//            }
//            self.image = image
//        } else {
//            self.image = nil
//        }
        
        self.telephoneNumber  = try container.decodeIfPresent(String.self, forKey: .telephoneNumber)
        self.uid  = try container.decodeIfPresent(String.self, forKey: .uid)
        self.name  = try container.decodeIfPresent(String.self, forKey: .name)
        self.age  = try container.decodeIfPresent(Int.self, forKey: .age)
        self.rating  = try container.decodeIfPresent(Double.self, forKey: .rating)
        self.tasksToComplete = try container.decodeIfPresent([TextTask].self, forKey: .tasksToComplete)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
//        if let image = self.image {
//            guard let data = image.jpegData(compressionQuality: 1) else {
//                return
//            }
//            try container.encode(data, forKey: .image)
//        }
        
        try container.encode(self.telephoneNumber, forKey: .telephoneNumber)
        try container.encode(self.uid, forKey: .uid)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.age, forKey: .age)
        try container.encode(self.rating, forKey: .rating)
        try container.encode(self.tasksToComplete, forKey: .tasksToComplete)
    }
    
    func getImage() -> UIImage? {
        if let image = self.image {
            return image
        } else {
            return nil
        }
    }
    
    func getTelephoneNumber() -> String? {
        return self.telephoneNumber ?? nil
    }
    
    func getUid() -> String? {
        return self.uid ?? nil
    }

    func getName() -> String? {
        return self.name ?? nil
    }
    
    func getAge() -> Int? {
        return self.age ?? nil
    }
    
    func getRating() -> Double? {
        return self.rating ?? nil
    }
    
    func getTasksToComplete() -> [TextTask]? {
        return self.tasksToComplete ?? nil
    }
    
    func appendTasksToComplete(task: TextTask) {
        if var tasksToComplete = self.tasksToComplete {
            tasksToComplete.append(task)
            self.tasksToComplete = tasksToComplete
        } else {
            let tasksToComplete = [task]
            self.tasksToComplete = tasksToComplete
        }
    }
    
}
